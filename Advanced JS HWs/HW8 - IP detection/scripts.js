const button = document.querySelector('.getData');
button.addEventListener('click', sendRequest);

async function sendRequest(e) {
        e.preventDefault();

        document.querySelector('.city-card') ? document.querySelector('.city-card').remove() : false;

        const responseIp = await fetch('https://api.ipify.org/?format=json');
        const ipInfo = await responseIp.json();
        const {ip} = ipInfo;

        const responseDetalization =
            await fetch(`http://ip-api.com/json/${ip}?lang=ru&fields=continent,country,regionName,city,district`);

        const ipDetalization = await responseDetalization.json();

        ipDetalization ? render(ipDetalization, button) : false;

}

function render({city= 'не получен', continent='не получен', country='не получен',
                    regionName='не получен', district='не получен'},
                    parent = document.body, location ='afterend') {

    district.length === 0 ? district = city : false;

    const innerHtml =
        `<div class="city-card">
            Континент: ${continent};<br>
            Страна: ${country};<br>
            Регион: ${regionName};<br>
            Город: ${city};<br>
            Район: ${district}.<br>
         </div>    
        `;

    parent.insertAdjacentHTML(location, innerHtml);
}