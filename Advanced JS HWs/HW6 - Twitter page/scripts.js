class CardList {
    static container = document.querySelector('.container');

    constructor() {
        this._addPublicationContainer();
        this._getData();
    }

    _addPublicationContainer() {
        const addPublicationButton = document.querySelector('.add-publication');
        addPublicationButton.addEventListener('click', () => {
            const container = document.querySelector('.container');
            const innerHtml  = `
            <div class="new-post-container">
                <input class="new-post-title" type="text" value="enter title">
                <input class="new-post-text" type="text" value="enter text">
                <button class="create-post-button">Create post</button>
            </div>

            `;
            container.insertAdjacentHTML('beforebegin', innerHtml);
            document.querySelector('.create-post-button').addEventListener('click', () => {
                const newPostTitle = document.querySelector('.new-post-title').value;
                const newPostBody = document.querySelector('.new-post-text').value;

                $.ajax({
                    url: `https://jsonplaceholder.typicode.com/posts`,
                    type: 'POST',
                    data: ({
                        title: newPostTitle,
                        body: newPostBody,
                        userId: 1
                    }),
                    success: function( data ) {

                        // backend return same id for one more new post

                        document.querySelector('.new-post-container').remove();
                        $.when(
                            $.getJSON('https://jsonplaceholder.typicode.com/users')
                        ).done(users => {
                            const postWithUser = {
                                ...data,
                                user: users.find(user => user.id === +data.userId)
                            };
                            new Card(data, users, postWithUser, 'afterend', addPublicationButton);
                            const cardList = new CardList().addPublicationContainer();
                        });
                    }
                })
            });
        }, {once:true})
    }

    _getData() {
        $.when(
            $.getJSON('https://jsonplaceholder.typicode.com/posts'),
            $.getJSON('https://jsonplaceholder.typicode.com/users')
        ).done(([posts],[users]) => {
            const arr = posts.forEach(post => {
                const postWithUser = {
                    ...post,
                    user: users.find(user => user.id === post.userId)
                };
                new Card(post, users, postWithUser);
            });
        });
    }
}

class Card {

    constructor(postData, users, cardData, place= 'beforeend', parent = CardList.container) {
        this._users = users;
        this._defaultData = postData;
        this.cardData = cardData;
        this._postInnerHtml = this._createInnerHtml(this.cardData, 'span');
        this.render( parent, this._postInnerHtml, place);
        this._card = document.querySelector(`.card-ID-${this.cardData.id}`);
        this._eventListener(this.cardData);
        this._createButton('Edit', 'edit-button', this._card);
        this._createButton('Delete', 'destroy-button', this._card);
    }

    _createInnerHtml({title, body, id, user:{name, username, email}}, tagName = 'span'){
            if( tagName === 'input'){
                return `<p class="card card-ID-${id}">
                    User Name: <span class="username">${username}.</span><br>
                    Name: <span  class="name">${name}.</span><br>
                    Email: <span  class="email">${email}.</span><br>
                    Title: <input value="${title}" class="title"><br>
                    Text: <input value="${body}" class="body"><br>
                </p>`;
            }

         return `<p class="card card-ID-${id}">
                    User Name: <${tagName} value="" class="username">${username}</${tagName}>.<br>
                    Name: <${tagName} class="name">${name}</${tagName}>.<br>
                    Email: <${tagName} class="email">${email}</${tagName}>.<br>
                    Title: <${tagName} class="title">${title}</${tagName}>.<br>
                    Text: <${tagName} class="body">${body}</${tagName}>.<br>
                </p>`;
    }

    render(parent, childHtml, place) {
        parent.insertAdjacentHTML(place, childHtml);
    }

    _createButton(innerText, className, parent) {
        const button = document.createElement('button');
        button.innerText = innerText;
        button.classList.add(className);
        parent.append(button);
    }

    _eventListener({id}) {
        this._card.addEventListener('click', (event) => {
            if(event.target.classList.contains('edit-button')) {
                this.edit();
            }
            if(event.target.classList.contains('destroy-button')) {
                this.destroy();
            }
        });
    }

    edit() {
        const parent = document.querySelector(`.card-ID-${this.cardData.id}`);

        parent.insertAdjacentHTML('beforebegin', this._createInnerHtml(this.cardData, 'input'));
        parent.classList.add('hidden');
        const newParent = document.querySelector(`.card-ID-${this.cardData.id}`);
        this._createButton('Save', 'save-button', newParent);
        this._createButton('Cancel', 'cancel-button', newParent);

        newParent.querySelector('.cancel-button').addEventListener('click', ()=> {
            parent.classList.remove('hidden');
            newParent.remove();
        });

        newParent.querySelector('.save-button').addEventListener('click', (e)=> {
            const newTitle = newParent.querySelector('.title').value;
            const newBody = newParent.querySelector('.body').value;
            const idValue = this._defaultData.id;
            $.ajax({
                url: `https://jsonplaceholder.typicode.com/posts/${idValue}`,
                type: 'PUT',
                data: JSON.stringify({
                    ...this._defaultData,
                    title: newTitle,
                    body: newBody
                }),
                success: function(data) {
                    parent.classList.remove('hidden');
                    parent.querySelector('.title').innerText = newTitle;
                    parent.querySelector('.body').innerText = newBody;
                    newParent.remove();
                }
            })
        });
    }

    destroy() {
        $.ajax({
            url: `https://jsonplaceholder.typicode.com/posts/${this.cardData.id}`,
            type: 'DELETE',
            data: JSON.stringify({
                ...this._defaultData
            }),
            success: function(data) {
            }
        })
            .then(()=> this._card.remove());

    }
}

const cardList = new CardList();