class Game {
    constructor() {
        this._levels = [['Low Level', 1500],['Medium Level', 1000],['Hard Level', 500]];
        this._speed = 0;
        this.choseSpeedLevel(this.startGame.bind(this));
        this._userCells = 0;
        this._computerCells = 0;
        this._counter = 0;
        this._arrOfCells = [];

    }

    choseSpeedLevel(startGame) {
        const parent = document.createElement('div');
        parent.id = 'speed-buttons-container';
        document.body.prepend(parent);

        this._levels.map(elem => {
            parent.insertAdjacentHTML('afterbegin',`<button id="${elem [0]}">${elem[0]}</button>`)
        });

        this.choseSpeedLevelListener(startGame, this._levels, parent, this._speed);

    }

    choseSpeedLevelListener(startGame, levelsName, parent){
        document.body.addEventListener('click', function speedListener(event) {
            let speedName = 0;
            let targetName = event.target.id;
            levelsName.forEach(function(elem) {
                if(elem.includes(targetName)) {
                    speedName = targetName;
                    document.body.removeEventListener('click', speedListener);
                    startGame(speedName);
                    parent.remove();
                }
            });
        });
    }

    startGame(speedName) {
        this.setSpeed(speedName);
        this.createField();
        this.getNodesOfCells();
        this.makeArrayFromNodeList();
        this.shuffleCellsForGame(this._arrOfCells);
        this.createCellInGame();
    }

    setSpeed(value) {
        this._levels.forEach(elem => {
            if(value === elem[0]){
                this._speed = elem[1];
            }
        });
    }

    createField() {
        if(!document.getElementById('container')){
            const container = document.createElement('div');
            container.id = 'container';
            document.body.append(container);
        }

        for (let i = 0; i < 100; i++){
            document.getElementById("container")
                .appendChild(document.createElement("td"))
                .classList.add('default-cell');
        }
    }

    getNodesOfCells() {
        this._arrOfCells = document.querySelectorAll('.default-cell');
    }

    makeArrayFromNodeList() {
        this._arrOfCells = Array.from(this._arrOfCells);
    }

    shuffleCellsForGame(arr) {
        for (let i = arr.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            [arr[i], arr[j]] = [arr[j], arr[i]];
        }
    }

    createCellInGame(){
        this.showScore();
        this.doWeHaveWinner();
        this._randomCell = this._arrOfCells[this._counter++];
        if(this._randomCell) {
            this._randomCell.classList.add('in-game');
            this.clickListener();
                setTimeout(() => {
                    this.createCellInGame();
                },this._speed);
        }
    }

    clickListener() {
        this._ifNoClick = setTimeout(this.giveCellToComputer.bind(this), this._speed);
        this._randomCell.addEventListener('click', () => {
            this.giveCellToUser();
            clearTimeout(this._ifNoClick);
        }, {once: true});
    }

    giveCellToUser() {
        this._randomCell.classList.remove('in-game');
        this._randomCell.classList.add('player-cell');
        this._userCells++;
    }

    giveCellToComputer() {
        this._randomCell.classList.remove('in-game');
        this._randomCell.classList.add('computer-cell');
        clearTimeout(this._ifNoClick);
        this._computerCells++;
    }

    showScore() {
        if (document.querySelector('.score')){
            document.querySelector('.score').remove();
        }

        const ul = document.createElement('ul');
        const playerScore = document.createElement('li');
        const computerScore = document.createElement('li');

        ul.classList.add('score');
        ul.id = 'score';

        playerScore.innerText = `Player score: ${this._userCells}`;
        computerScore.innerText = `Computer score: ${this._computerCells}`;

        document.body.append(ul);
        ul.append(playerScore);
        ul.append(computerScore);
    }

    doWeHaveWinner() {
        if (this._userCells === 50 || this._computerCells === 50) {
            const winnerName = (this._userCells > this._computerCells)? 'player': 'computer';
            alert(`Winner is ${winnerName}`);
            this.deleteGame();

            const game = new Game;
        }

    }

    deleteGame() {
        document.getElementById('score').remove();
        document.getElementById('container'). remove();
        this._arrOfCells = [];
    }
}

const game = new Game;
