class FilmsList {
    constructor(link) {
        this._container = document.querySelector('.container');
        this.getData(link);
    }

    getData(link) {
        fetch(link)
            .then(response => response.json())
            .then(data => {
                    this._data = data;
                    this.getFilms(this._data);
            });
    }

    getFilms({results}) {
       if(results) {
           results.forEach(({title, episode_id, opening_crawl, characters}) => {
               const ul = `
                   <ul id="${title}">Episode name: ${title}.<br>
                   Episode number: ${episode_id}.<br>
                   Episode description: ${opening_crawl}.<br>
                   </ul>`;
               this._container.insertAdjacentHTML('afterbegin',ul);

               const parent = document.getElementById(title);
               const loading = document.createElement('li');
               loading.innerText = 'Loading...';
               loading.classList.add('remove');
               parent.append(loading);

               this.renderFilms(characters, parent, loading);
           });
       }
    }

    renderFilms(characters, parent, loading) {
        const actorsArr = [];

        const requests = characters.map(url => fetch(url));

        Promise.all(requests)
            .then(responses => Promise.all(responses.map(response => response.json()))
            .then(data => data.forEach(({name}) => actorsArr.push(`<li>${name}</li>`))))
            .then(() => {
                loading.remove();
                parent.insertAdjacentHTML('beforeend', actorsArr.join(''));
        });


        
    }
}

const filmList = new FilmsList('https://swapi.dev/api/films/');