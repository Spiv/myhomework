class List {
    constructor() {
        this.createNewColumn();
        this.createContainer();
        this.addCardListener();
    }

    createNewColumn() {
        this._createColumnBtn = document.createElement('button');
        this._createColumnBtn.id = 'add-column-button';
        this._createColumnBtn.innerText = 'Add new column';
        document.body.append(this._createColumnBtn);
        this.newColumnButtonListener();
    }

    newColumnButtonListener() {
        this._createColumnBtn.addEventListener('click', this.addNewColumn);
    }

    createContainer() {
        const container = document.createElement('div');
        container.classList.add('container');
        document.body.append(container);
    }

    addNewColumn() {
        const column = document.createElement('div');
        const parent = document.querySelector('.container');
        const cardsContainer = document.createElement('div');
        const buttonAddCard = document.createElement('button');
        const buttonSort = document.createElement('button');

        buttonAddCard.innerText = 'Add card';
        buttonSort.innerText = 'Sort cards';

        buttonAddCard.classList.add('add-card-btn');
        buttonSort.classList.add('sort-cards-btn');
        column.classList.add('column');
        cardsContainer.classList.add('cards-container');

        parent.append(column);
        column.append(cardsContainer);
        column.append(buttonAddCard);
        column.append(buttonSort);
    }

    addCardListener() {
        const parent = document.querySelector('.container');
        parent.addEventListener('click', (e) => {
            if(e.target.classList.contains('add-card-btn')) {
                this.addCard(e.target);
            }
            if(e.target.classList.contains('sort-cards-btn')){
                this.sortCardsInColumn(e.target);
            }
        });
    }

    /*
    * onDropCreatedCard = boolean;
    */

    addCard(target, onDropCreatedCard) {
        const card = document.createElement('div');
        card.classList.add('card');

        if(onDropCreatedCard) {
            parent = target;
            card.innerHTML = event.dataTransfer.getData("text/html");
            parent.before(card);
        } else {
            parent = target.parentNode.firstElementChild;
            parent.append(card);
        }

        card.addEventListener('click', this.cardChangeText);
        this.dragDrop(card);
    }

    cardChangeText() {
        if(document.getElementById('change-card-text')) {
            document.getElementById('change-card-text').remove();
        }

        const inputContainer = document.createElement('div');
        inputContainer.innerHTML = `<input id='change-card-text' placeholder='Изменить текст карточки' value='${this.innerText}'>`;
        document.body.insertAdjacentHTML('beforeend', inputContainer.innerHTML);
        const input = document.getElementById('change-card-text');
        input.onblur = ({target}) => {
            this.innerText = target.value;
            target.remove();
        }
    }

    dragDrop(card){
        card.draggable = 'true';
        card.addEventListener('dragstart', this.onDragStart);
        card.addEventListener('drop', this.onDrop.bind(this));
        card.addEventListener('dragover', this.onDragOver);
    }

    onDragOver(event) {
        event.preventDefault();
    }

    onDragStart(event) {
        event.dataTransfer.setData("text/html", event.target.innerHTML);
        this.id = 'for-remove';
    }

    onDrop(event) {
        event.preventDefault();
        this.addCard(event.target, true);

        if(document.getElementById('for-remove')) {
            document.getElementById('for-remove').remove();
        }
    }

    sortCardsInColumn(event) {
        const CardsNodeList = Array.from(event.parentNode.querySelectorAll('.card'));
        const CardsInnerText = CardsNodeList.map(elem => elem.innerText);
        const sortedCards = CardsInnerText.sort(function(a,b) {
            if(+a - +b)
                return a - b;
            if (a < b)
                return -1;
            if (a > b)
                return 1;
            return 0
        });
        for(let i = 0; i < CardsNodeList.length; i++) {
            CardsNodeList[i].innerText = sortedCards[i];
        }
    }
}

const list = new List();
