const contactUsBtn = document.querySelector('.row9-button-contact-us');

contactUsBtn.addEventListener('click', () => {

    if( get_cookie('new-user') === null) {
        document.cookie = 'new-user=true';
    } else if (get_cookie('new-user')){
        document.cookie = 'new-user=false';
    }  else {
        throw new Error('Something goes wrong!!!');
    }

    document.cookie = 'experiment=novalue';

    get_cookie('new-user');
    get_cookie('experiment');
    delete_cookie('experiment', 300);
});

function get_cookie ( cookie_name ) {
    let cookies = document.cookie
        .split(';')
        .map( keyvalue => keyvalue.split('=').map(str => str.trim()));

    cookies = Object.fromEntries(cookies);

    return cookies[cookie_name] ?  cookies[cookie_name] :  null;
}

function delete_cookie ( cookie_name, valueInSeconds = 0) {
    const cookie_date = new Date ( );
    cookie_date.setTime ( cookie_date.getTime() + valueInSeconds * 1000 );
    document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString();
}