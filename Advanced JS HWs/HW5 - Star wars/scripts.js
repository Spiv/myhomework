class List {
    constructor(link) {
        this._container = document.querySelector('.container');
        this.getData(link);
    }

    getData(link) {
        const request = new XMLHttpRequest();
        request.open('GET', link);
        request.responseType = "json";
        request.send();
        request.onload = () => {
            if(request.status !==200) {
                throw new Error('Something goes wrong');
            }
            this._data = request.response;
            this.getFilms(this._data);
        };
    }

    getFilms({results}) {
       if(results) {
           results.forEach(({title, episode_id, opening_crawl, characters}) => {
               const ul = `
                   <ul id="${title}">Episode name: ${title}.<br>
                   Episode number: ${episode_id}.<br>
                   Episode description: ${opening_crawl}.<br>
                   </ul>`;
               this._container.insertAdjacentHTML('afterbegin',ul);

               const parent = document.getElementById(title);
               const loading = document.createElement('li');
               loading.innerText = 'Loading...';
               loading.classList.add('remove');
               parent.append(loading);

               const films = new Films(characters, parent, loading);
           });
       }
    }
}

class Films{
    constructor(characters, parent, loading) {
        this.getFilms(characters, parent, loading);
    }

    getFilms(characters, parent, loading) {
        const actorsArr = [];

        characters.forEach( elem => {
            const request = new XMLHttpRequest();
            request.open('GET', elem);
            request.send();

            request.onload = function() {
                const li = `<li>${JSON.parse(request.response).name}</li>`;
                actorsArr.push(li);

                if(actorsArr.length === characters.length){
                    loading.remove();
                    parent.insertAdjacentHTML('beforeend', actorsArr.join(''));
                }
            };
        });
    }
}

const list = new List('https://swapi.dev/api/films/');