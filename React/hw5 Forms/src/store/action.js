import axios from "axios";

export const loadDataAction = () => dispatch => {
   axios('/data.json')
    .then(res => {
      dispatch({type: 'SAVE_CARDS', payload: res.data});
    })
};

export const toggleFirstModalAction = () => dispatch => dispatch({type: 'TOGGLE_FIRST_MODAL'});

export const toggleSecondModalAction = () => dispatch => dispatch({type: 'TOGGLE_SECOND_MODAL'});

export const setAddingInBasketCodeAction = (id) => dispatch => dispatch({type: 'SET_ADDING_IN_BASKET_CODE', payload: id});

export const setRemovingFromBasketCodeAction = (id) => dispatch => dispatch({type: 'SET_REMOVING_FROM_BASKET_CODE', payload: id});

export const changeProductsInBasketAction = (products) => dispatch => dispatch({type: 'CHANGE_PRODUCTS_IN_BASKET', payload: products});

export const changeFavoriteProductsAction = (products) => dispatch => dispatch({type: 'CHANGE_FAVORITE_PRODUCTS', payload: products});

export const basketCleaningAction = () => dispatch => dispatch({type: 'BASKET_CLEANING'});

export const setFormValuesAction = (values) => dispatch => dispatch({type: 'SET_FORM_VALUES', payload: values});