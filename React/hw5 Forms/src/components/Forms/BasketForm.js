import React from 'react'
import {Form, Field, Formik, ErrorMessage} from 'formik';
import * as yup from 'yup';
import './BasketForm.scss';
import {basketCleaningAction} from "../../store/action";
import {useDispatch, useSelector} from "react-redux";
import {getBasket, getCardsSelector, getFormValues} from "../../store/selectors/selectors";
import {setLocalStorageArray} from '../../functions/functions';
import {setFormValuesAction} from "../../store/action";

const formSchema = yup.object().shape({
  name: yup
    .string()
    .required("This field is required")
    .min(2, "Minimum length is 3 symbols"),
  surname: yup
    .string()
    .required("This field is required")
    .min(3, "Minimum length is 3 symbols"),
  age: yup
    .number()
    .integer()
    .positive()
    .moreThan(18, 'Not younger than 18 years')
    .required("This field is required"),
  address: yup
    .string()
    .required("This field is required")
    .min(5, "Minimum length is 5 symbols"),
  phone: yup
    .number()
    .integer()
    .positive()
    .required("This field is required")
});

const BasketForm = () => {
  const dispatch = useDispatch();
  const basket = useSelector(getBasket);
  const productsData = useSelector(getCardsSelector);
  const formValues = useSelector(getFormValues);

  const basketCleaning = () => dispatch(basketCleaningAction());
  const setFormValues = (values) => dispatch(setFormValuesAction(values));

  const handleSubmit = (values, {setSubmitting}) => {
    setSubmitting(false);
    console.log('Form data', values);

    const purchasedGoods = [];
    productsData.map( card => {
      basket.forEach( code => {
        if(card.code === code){
          purchasedGoods.push(card);
        }
      })
    });
    console.log('Purchased good for post axios', purchasedGoods);
    basketCleaning();
    setLocalStorageArray('basket');
    setFormValues({
      name: '',
      surname: '',
      age: '',
      address: '',
      phone: ''
    });
  };

  return (
    <Formik
      initialValues = {{
        name: formValues.name,
        surname: formValues.surname,
        age: formValues.age,
        address: formValues.address,
        phone: formValues.phone
      }}
      validationSchema={formSchema}
      onSubmit={handleSubmit}
    >

      {(formikProps) => {

        setFormValues(formikProps.values);

        return (
          <Form onSubmit={formikProps.handleSubmit} noValidate className='basket__form'>
            <h3>Оформить заказ</h3>
            <Field component='input' type='text' name='name' placeholder='Name'/>
            <span className='error'><ErrorMessage name='name' /></span>
            <Field component='input' type='text' name='surname' placeholder='Surname'/>
            <span className='error'><ErrorMessage name='surname' /></span>
            <Field component='input' type='number' name='age' placeholder='Age'/>
            <span className='error'><ErrorMessage name='age' /></span>
            <Field component='input' type='text' name='address' placeholder='Address'/>
            <span className='error'><ErrorMessage name='address' /></span>
            <Field component='input' type='text' name='phone' placeholder='Phone'/>
            <span className='error'><ErrorMessage name='phone' /></span>
            <div>
              <button className='basket__submit-button' type='submit' disabled={formikProps.isSubmitting}>Оформить заказ</button>
            </div>
          </Form>
        )
      }}
    </Formik>
  )
};

export default BasketForm;