import React from 'react';
import './Header.scss';
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import {getBasket, getFavoriteProducts} from "../../store/selectors/selectors";

const Header = () => {
  const basket = useSelector(getBasket);
  const favoriteProducts = useSelector(getFavoriteProducts);

  return (
    <div className='header'>
      <div className='header__statistic'>
        <span>В корзине товаров: {basket.length} шт.</span><br/>
        <span>В избранном товаров: {favoriteProducts.length} шт.</span>
      </div>

      <nav>
        <Link to='/'>На главную</Link> <br/>
        <Link to='/favorites'>К избранным</Link> <br/>
        <Link to='/basket'>В корзину</Link>
      </nav>
    </div>
  );
};

export default Header;