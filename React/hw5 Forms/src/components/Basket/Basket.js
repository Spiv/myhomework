import React from "react";
import List from "../../pages/List/List";
import {useSelector} from "react-redux";
import {getBasket} from "../../store/selectors/selectors";
import BasketForm from "../Forms/BasketForm";

const Basket = () => {
  const basket = useSelector(getBasket);

  return (
    <>
      {basket.length > 0 && <BasketForm/>}
      <List cardsCodes={basket}/>
    </>
  )
};

export default Basket;
