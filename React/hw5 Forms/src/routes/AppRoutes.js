import React from 'react';
import {useSelector} from "react-redux";
import { Route, Switch } from 'react-router-dom';
import List from "../pages/List/List";
import Basket from "../components/Basket/Basket";
import {getFavoriteProducts, getCardsSelector} from "../store/selectors/selectors";

const AppRoutes = () => {
  const productsData = useSelector(getCardsSelector);
  const favoriteProducts = useSelector(getFavoriteProducts);

  const allCodes = [];
  productsData.map( card => allCodes.push(card.code));

  return (
    <>
      <Switch>
        <Route exect path='/basket' render={()=> <Basket/>}/>
        <Route exect path='/favorites' render={()=> <List cardsCodes={favoriteProducts}/>}/>
        <Route path='/' render={()=> <List cardsCodes={allCodes}/>}/>
      </Switch>
    </>
  );
};

export default AppRoutes;