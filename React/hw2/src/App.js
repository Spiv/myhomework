import React, {Component} from 'react';
import './App.css';
import Body from "./components/Body/Body";
import Header from "./components/Header/Header";
import axios from 'axios';

class App extends Component {

    state = {
        modalOpen: false,
        addingInBasketCode: null,
        productsData: [],
        favoriteProducts: [],
        basket: []
    };

    componentDidMount() {
        axios('/data.json')
            .then(res => this.setState((lastState) => {
                return {...lastState, productsData: [...res.data]}
            }))
    }

    setLocalStorageArray = (name) => {
        localStorage.setItem(name, JSON.stringify([]));
        return [];
    };

    showFirstModal = (productId) => {
        // eslint-disable-next-line no-lone-blocks
        {productId && this.setState({addingInBasketCode: productId})}
        this.setState({modalOpen: !this.state.modalOpen});
    };

    changeStatus = (productID, value, needModal=true) => {
        (value === 'basket' && needModal && this.setState((lastState) => {
                return {
                    ...lastState,
                    modalOpen: !this.state.modalOpen
                }
            })
        );

        const values = JSON.parse(localStorage.getItem(value));

        if( values === null) {
            const arr = [];
            arr.push(productID);
            localStorage.setItem(value, JSON.stringify(arr));
            this.setState((lastState) => {
                return { ...lastState, [value]: arr }
            });
            return arr;
        }

            if(values.findIndex(elem => elem === productID) !== -1 && values.length !== 0) {
                const index = values.findIndex(elem => elem === productID);
                values.splice(index, 1);
                localStorage.setItem(value, JSON.stringify(values));
            } else {
                values.push(productID);
                localStorage.setItem(value, JSON.stringify(values));
            }

        this.setState((lastState) => {
            return { ...lastState, [value]: values }
        })
    };

    render() {
        return <>
            <Header setLocalStorageArrays={this.setLocalStorageArray}/>
            <Body changeStatus={this.changeStatus} showFirstModal={this.showFirstModal} data={this.state}/>
            </>
    }
}

export default App;
