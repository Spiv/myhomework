import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Button extends Component {
    render() {
        const {description, onClickFunc = () => console.log(new Error('No func!!!'))} = this.props;
        const {backgroundColor, id, text} = description;

        return (
            <button key={id} onClick={() => onClickFunc()} style={{backgroundColor: backgroundColor }}>{text} </button>
        );
    }
}

Button.propTypes = {
    description: PropTypes.object,
    onClickFunc: PropTypes.func,
    backgroundColor: PropTypes.string,
    id: PropTypes.string,
    text: PropTypes.string
};

export default Button;