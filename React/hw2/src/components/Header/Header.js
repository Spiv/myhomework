import React, {Component} from 'react';
import './Header.scss';

class Header extends Component {
    render() {
        const {setLocalStorageArrays} = this.props;

        const chosenProducts = () => {
            let inFavoriteAmount, inBasketAmount;

            const inFavorite = JSON.parse(localStorage.getItem('favoriteProducts'));
            const inBasket = JSON.parse(localStorage.getItem('basket'));

            inFavorite === null ?
                inFavoriteAmount = setLocalStorageArrays('favoriteProducts') :
                inFavoriteAmount = inFavorite.length;

            inBasket === null ?
                inBasketAmount = setLocalStorageArrays('basket'):
                inBasketAmount = inBasket.length;

            return <div className='header__statistic'>
                <span>В корзине товаров: {inBasketAmount} шт.</span><br/>
                <span>В избранном товаров: {inFavoriteAmount} шт.</span>
            </div>
        };

        return (
            <>
                {chosenProducts()}
            </>
        );
    }
}

export default Header;