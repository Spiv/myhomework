import React, {Component} from 'react';
import '../ProductCard/ProductCard.scss'
import Button from "../Button/Button";
import PropTypes from 'prop-types';

class ProductCard extends Component {
    render() {
        const {data, showFirstModal, addToBasketButton, changeStatus} = this.props;
        const {code, color, link, price, title} = data;

        const favoriteProducts = JSON.parse(localStorage.getItem('favoriteProducts'));
        const favoriteProduct = favoriteProducts.find(elem => elem === code) ? "product__star product__star-favorite" : "product__star";

        let productInBasket;
        const basket = JSON.parse(localStorage.getItem('basket'));
        basket.find(elem => elem === code) ? productInBasket = false: productInBasket = true;

        return (
            <>
                <div className='product__card'>
                    <a href={link}>
                        <h5 className='product__title'>Модель: {title}</h5>
                    </a>
                    <button onClick={() => changeStatus(code, 'favoriteProducts')} className={favoriteProduct} >&#9734;</button>
                    {!productInBasket && <i onClick={() => changeStatus(code, 'basket', false)} className="fas fa-shopping-basket"/>}
                    <img className='product__img' src={link} alt="car"/>
                    <p>Цена: {price}$</p>
                    <p>Цвет: {color}</p>
                    {productInBasket && <Button onClickFunc={() => showFirstModal(code)} description={addToBasketButton}/>}
                </div>
                </>
        );
    }
}

ProductCard.propTypes = {
    data: PropTypes.object,
    showFirstModal: PropTypes.func,
    addToBasketButton: PropTypes.object,
    addToFavorite: PropTypes.func,
    code: PropTypes.string ,
    color: PropTypes.string ,
    link: PropTypes.string ,
    price: PropTypes.number,
    title: PropTypes.string
};

export default ProductCard;