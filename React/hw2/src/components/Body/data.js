const modalsData = {
            addToBasketButton: {
            name: 'firstButton',
            id: 'firstButtonId',
            backgroundColor: '#8c5289',
            text: 'Добавить в корзину',
            modal: {
                header: 'Вы добавили товар в корзину!',
                headerBackgroundColor: '#733c8c',
                closeButton: true,
                backgroundColor: '#8c5289',
                text: `Если вы передумали, то нажмите кнопку "Отменить"`,
                actions: [
                    {
                        id: 'okButton',
                        text: 'OK',
                        backgroundColor: '#4c285b'
                    },
                    {
                        id: 'cancelButton',
                        text: 'Отменить',
                        backgroundColor: '#4c285b'
                    }
                ]
            }
        }
};

export default modalsData;
