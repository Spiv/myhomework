import React, {Component} from 'react';
import Button from "../Button/Button";
import PropTypes from 'prop-types';
import Modal from "../../pages/Modal/Modal";
import modalsData from '../Body/data';
import ProductCard from "../ProductCard/ProductCard";
import '../ProductCard/ProductCard.scss';

class Body extends Component {
    render() {

        const {changeStatus, showFirstModal, data} = this.props;
        const {modalOpen, addingInBasketCode, productsData} = data;
        const {addToBasketButton} = modalsData;

        const productsCards = productsData.map(data => {
            return <ProductCard addToBasketButton={addToBasketButton}
                                changeStatus={changeStatus}
                                showFirstModal={showFirstModal}
                                data={data}
                                key={data.code}
            />
            });

        // eslint-disable-next-line array-callback-return
        const modalAdditionalButtons = addToBasketButton.modal.actions.map(button => {
            if(button.id === 'okButton'){
                return <Button onClickFunc={() => changeStatus(addingInBasketCode, 'basket')}
                               key={button.id}
                               description={button}/>
            }
            if(button.id === 'cancelButton') {
                return <Button onClickFunc={() => showFirstModal()} key={button.id} description={button}/>
            }
        });

        return (
            <>
                {modalOpen && <Modal modal={addToBasketButton.modal}
                                     onClickFunc={showFirstModal}
                                     additionalButtons={modalAdditionalButtons}
                />}
                <div className='cards__container'>
                    {productsCards}
                </div>
            </>
        );
    }
}

Body.propTypes = {
    addToFavorite: PropTypes.func,
    addToBasket: PropTypes.func,
    showFirstModal: PropTypes.func,
    data: PropTypes.object,
    modalOpen: PropTypes.func,
    addingInBasketCode: PropTypes.string,
    productsData: PropTypes.object,
    addToBasketButton: PropTypes.object
};

export default Body;