import React from 'react';
import { Route, Switch } from 'react-router-dom';
import List from "../pages/List/List";

const AppRoutes = (props) => {
  const {basket, favoriteProducts, allCodes} = props;

  return (
    <>
      <Switch>
        <Route exect path='/products' render={()=> <List data={props} cardsCodes={allCodes}/>}/>
        <Route exect path='/basket' render={()=> <List data={props} cardsCodes={basket}/>}/>
        <Route exect path='/favorites' render={()=> <List data={props} cardsCodes={favoriteProducts}/>}/>
      </Switch>
    </>
  );
};

export default AppRoutes;