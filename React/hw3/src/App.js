import React, {useState, useEffect} from 'react';
import './App.css';
import Header from "./components/Header/Header";
import axios from 'axios';
import AppRoutes from "./routes/AppRoutes";

const App = () => {
  const [firstModalOpen, setFirstModalOpen] = useState(false);
  const [secondModalOpen, setSecondModalOpen] = useState(false);
  const [addingInBasketCode, setAddingInBasketCode] = useState(null);
  const [removingFromBasketCode, setRemovingFromBasketCode] = useState(null);
  const [productsData, setProductsData] = useState([]);
  const [favoriteProducts, setFavoriteProducts] = useState([]);
  const [basket, setBasket] = useState([]);
  const [allCodes, setAllCodes] = useState([]);


  useEffect (() => {
    axios('/data.json')
      .then(res => {
        setProductsData(res.data);

        const arr = [];
        res.data.forEach( elem => arr.push(elem.code));
        setAllCodes(arr);
      })
  }, []);

  useEffect(() => {
    let storageBasket = JSON.parse(localStorage.getItem('basket'));
    let storageFavoriteProducts = JSON.parse(localStorage.getItem('favoriteProducts'));

    if(storageBasket === null) {
      storageBasket = [];
      setLocalStorageArray('basket')
    }

    if(storageFavoriteProducts === null) {
      storageFavoriteProducts = [];
      setLocalStorageArray('favoriteProducts')
    }

    setBasket(storageBasket);
    setFavoriteProducts(storageFavoriteProducts);
  }, []);

  const setLocalStorageArray = (name) => {
    localStorage.setItem(name, JSON.stringify([]));
    return [];
  };

  const showFirstModal = (productId) => {
    {productId &&  setAddingInBasketCode(productId)}
    setFirstModalOpen(!firstModalOpen);
    setRemovingFromBasketCode(null);
  };

  const showSecondModal = (productId) => {
    {productId &&  setRemovingFromBasketCode(productId)}
    setSecondModalOpen(!secondModalOpen);
    setAddingInBasketCode(null);
    setFirstModalOpen(false);
  };

  const changeStatus = (productID, value, needModal= true) => {
    (value === 'basket' && needModal && setFirstModalOpen(!firstModalOpen));

    const values = JSON.parse(localStorage.getItem(value));

    if(values.findIndex(elem => elem === productID) !== -1 && values.length !== 0) {
      const index = values.findIndex(elem => elem === productID);
      values.splice(index, 1);
      localStorage.setItem(value, JSON.stringify(values));
    } else {
      values.push(productID);
      localStorage.setItem(value, JSON.stringify(values));
    }

    value === 'basket' && setBasket(values);
    value === 'favoriteProducts' && setFavoriteProducts(values);
  };

  return <>
          <Header setLocalStorageArrays={setLocalStorageArray} favoriteProducts={favoriteProducts} basket={basket}/>
          <AppRoutes addingInBasketCode={addingInBasketCode}
                     removingFromBasketCode={removingFromBasketCode}
                     changeStatus={changeStatus}
                     showFirstModal={showFirstModal}
                     showSecondModal={showSecondModal}
                     firstModalOpen={firstModalOpen}
                     secondModalOpen={secondModalOpen}
                     basket={basket}
                     productsData={productsData}
                     favoriteProducts={favoriteProducts}
                     allCodes={allCodes}
          />
        </>
};

export default App;
