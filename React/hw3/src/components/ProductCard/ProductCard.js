import React from 'react';
import '../ProductCard/ProductCard.scss'
import Button from "../Button/Button";
import PropTypes from 'prop-types';

const ProductCard = (props) => {
  const {data, stared, productInBasket, showFirstModal, showSecondModal, addToBasketButton, changeStatus} = props;
  const {code, color, link, price, title} = data;



  return (
    <>
      <div className='product__card'>
        <a href={link}>
          <h5 className='product__title'>Модель: {title}</h5>
        </a>
        <button onClick={() => changeStatus(code, 'favoriteProducts')} className={stared} >&#9734;</button>
          {!productInBasket && <i onClick={() => showSecondModal(code)} className="far fa-times-circle"/>}
          <img className='product__img' src={link} alt="car"/>
          <p>Цена: {price}$</p>
          <p>Цвет: {color}</p>
          {productInBasket && <Button onClickFunc={() => showFirstModal(code)} description={addToBasketButton}/>}
      </div>
    </>
  );
};

ProductCard.propTypes = {
    data: PropTypes.object,
    showFirstModal: PropTypes.func,
    addToBasketButton: PropTypes.object,
    addToFavorite: PropTypes.func,
    code: PropTypes.string ,
    color: PropTypes.string ,
    link: PropTypes.string ,
    price: PropTypes.number,
    title: PropTypes.string
};

export default ProductCard;