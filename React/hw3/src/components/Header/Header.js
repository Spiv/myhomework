import React from 'react';
import './Header.scss';
import {Link} from "react-router-dom";

const Header = (props) => {
  const {basket, favoriteProducts} = props;

  return (
    <div className='header'>
      <div className='header__statistic'>
        <span>В корзине товаров: {basket.length} шт.</span><br/>
        <span>В избранном товаров: {favoriteProducts.length} шт.</span>
      </div>

      <nav>
        <Link to='/products'>На главную</Link> <br/>
        <Link to='/favorites'>К избранным</Link> <br/>
        <Link to='/basket'>В корзину</Link>
      </nav>
    </div>
  );
};

export default Header;