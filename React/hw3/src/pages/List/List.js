import React from 'react';
import ProductCard from "../../components/ProductCard/ProductCard";
import modalsData from "./data";
import Button from "../../components/Button/Button";
import Modal from "../Modal/Modal";

const List = (props) => {
  const {cardsCodes} = props;
  const {basket, favoriteProducts, changeStatus, showFirstModal, firstModalOpen, secondModalOpen, productsData, addingInBasketCode, removingFromBasketCode, showSecondModal} = props.data;
  const {addToBasketButton, removeFromBasketButton} = modalsData;

  const productsCards = () => {
    return productsData.map(data => {
      if(cardsCodes.find(elem => elem === data.code)) {
        const stared = favoriteProducts.find(elem => elem === data.code) ? "product__star product__star-favorite" : "product__star";

        let productInBasket;
        basket.find(elem => elem === data.code) ? productInBasket = false: productInBasket = true;

        return <ProductCard addToBasketButton={addToBasketButton}
                            changeStatus={changeStatus}
                            showFirstModal={showFirstModal}
                            showSecondModal={showSecondModal}
                            favoriteProducts={favoriteProducts}
                            basket={basket}
                            data={data}
                            key={data.code}
                            productInBasket={productInBasket}
                            stared={stared}
        />
      }
    });
  };

  const firstModalAdditionalButtons = addToBasketButton.modal.actions.map(button => {
    switch (button.id) {
      case "okButton":
        return <Button onClickFunc={() => changeStatus(addingInBasketCode, 'basket')}
                       key={button.id}
                       description={button}/>;
      case "cancelButton":
        return <Button onClickFunc={() => showFirstModal()} key={button.id} description={button}/>
    }
  });

  const secondModalAdditionalButtons = removeFromBasketButton.modal.actions.map(button => {
    switch (button.id) {
      case "okButton":
        return <Button onClickFunc={() => {
                         changeStatus(removingFromBasketCode, 'basket');
                         showSecondModal();
                       }}
                       key={button.id}
                       description={button}/>;
      case "cancelButton":
        return <Button onClickFunc={() => showSecondModal()} key={button.id} description={button}/>
    }
  });

    return (
        <div className='cards__container'>
            {productsCards()}
          {firstModalOpen && <Modal modal={addToBasketButton.modal}
                                    onClickFunc={showFirstModal}
                                    additionalButtons={firstModalAdditionalButtons}
          />}
          {secondModalOpen && <Modal modal={removeFromBasketButton.modal}
                                     onClickFunc={showSecondModal}
                                     additionalButtons={secondModalAdditionalButtons}
          />}
        </div>
    );

};

export default List;
