import React from 'react';
import './Modal.scss';
import PropTypes from 'prop-types';
import '../../components/Button/Button.scss';

const Modal = (props) => {
  const {modal, onClickFunc, additionalButtons} = props;
  const {header, headerBackgroundColor,closeButton, text, backgroundColor} = modal;

  return (
    <>
        <div className='modal__container' onClick={() => onClickFunc()}/>
        <div className='modal__window' style={{backgroundColor: backgroundColor }}>
            <h3 style={{backgroundColor: headerBackgroundColor }} className='modal__title'>{header}</h3>
            <div className='modal__content'>
                <p >{text}</p>
                {additionalButtons}
            </div>
            {closeButton && <button className='modal__close-btn pure-button' onClick={() => onClickFunc()}/>}
        </div>
    </>
  );
};

Modal.propTypes = {
    modal: PropTypes.object,
    showModal: PropTypes.func,
    name: PropTypes.string,
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    backgroundColor: PropTypes.string
};

export default Modal;