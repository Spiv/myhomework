import React from 'react';
import ProductCard from "../../components/ProductCard/ProductCard";
import modalsData from "./data";
import Button from "../../components/Button/Button";
import Modal from "../Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import { getAddingInBasketCode, getBasket, getCardsSelector, getFavoriteProducts, getFirstModalOpen,
  getRemovingFromBasketCode, getSecondModalOpen } from "../../store/selectors/selectors";
import {
  changeFavoriteProductsAction,
  changeProductsInBasketAction,
  toggleFirstModalAction,
  toggleSecondModalAction
} from "../../store/action";
import {setAddingInBasketCodeAction, setRemovingFromBasketCodeAction} from "../../store/action";

const List = (props) => {
  const {cardsCodes} = props;
  const basket = useSelector(getBasket);

  const {addToBasketButton, removeFromBasketButton} = modalsData;
  const productsData = useSelector(getCardsSelector);
  const firstModalOpen = useSelector(getFirstModalOpen);
  const secondModalOpen = useSelector(getSecondModalOpen);
  const addingInBasketCode = useSelector(getAddingInBasketCode);
  const removingFromBasketCode = useSelector(getRemovingFromBasketCode);
  const favoriteProducts = useSelector(getFavoriteProducts);
  const dispatch = useDispatch();

  const toggleFirstModal = () => dispatch(toggleFirstModalAction());

  const toggleSecondModal = () => dispatch(toggleSecondModalAction());

  const setAddingInBasketCode = (id) => dispatch(setAddingInBasketCodeAction(id));

  const setRemovingFromBasketCode = (id) => dispatch(setRemovingFromBasketCodeAction(id));

  const showFirstModal = (productId) => {
    {productId &&  setAddingInBasketCode(productId)}
    toggleFirstModal();
    setRemovingFromBasketCode(null);
  };

  const showSecondModal = (productId) => {
    {productId &&  setRemovingFromBasketCode(productId)}
    toggleSecondModal();
    setAddingInBasketCode('');
  };

  const changeProductsInBasket = products => dispatch(changeProductsInBasketAction(products));

  const changeFavoriteProducts = products => dispatch(changeFavoriteProductsAction(products));

  const changeStatus = (productID, value) => {
    const values = JSON.parse(localStorage.getItem(value));

    if(values.findIndex(elem => elem === productID) !== -1 && values.length !== 0) {
      const index = values.findIndex(elem => elem === productID);
      values.splice(index, 1);
      localStorage.setItem(value, JSON.stringify(values));
    } else {
      values.push(productID);
      localStorage.setItem(value, JSON.stringify(values));
    }

    value === 'basket' && changeProductsInBasket(values);
    value === 'favoriteProducts' && changeFavoriteProducts(values);
  };

  const productsCards = () => {
    return productsData.map(data => {
      if(cardsCodes.find(elem => elem === data.code)) {
        const stared = favoriteProducts.find(elem => elem === data.code) ? "product__star product__star-favorite" : "product__star";

        let isProductInBasket;
        basket.find(elem => elem === data.code) ? isProductInBasket = false: isProductInBasket = true;

        return <ProductCard addToBasketButton={addToBasketButton}
                            changeStatus={changeStatus}
                            showFirstModal={showFirstModal}
                            showSecondModal={showSecondModal}
                            data={data}
                            key={data.code}
                            isProductInBasket={isProductInBasket}
                            stared={stared}
        />
      }
    });
  };

  const firstModalAdditionalButtons = addToBasketButton.modal.actions.map(button => {
    switch (button.id) {
      case "okButton":
        return <Button onClickFunc={() => {
          changeStatus(addingInBasketCode, 'basket');
          toggleFirstModal();
        }}
                       key={button.id}
                       description={button}/>;
      case "cancelButton":
        return <Button onClickFunc={() => showFirstModal()} key={button.id} description={button}/>
    }
  });

  const secondModalAdditionalButtons = removeFromBasketButton.modal.actions.map(button => {
    switch (button.id) {
      case "okButton":
        return <Button onClickFunc={() => {
                         changeStatus(removingFromBasketCode, 'basket');
                         showSecondModal();
                       }}
                       key={button.id}
                       description={button}/>;
      case "cancelButton":
        return <Button onClickFunc={() => showSecondModal()} key={button.id} description={button}/>
    }
  });

    return (
        <div className='cards__container'>
            {productsCards()}
          {firstModalOpen && <Modal modal={addToBasketButton.modal}
                                    onClickFunc={showFirstModal}
                                    additionalButtons={firstModalAdditionalButtons}
          />}
          {secondModalOpen && <Modal modal={removeFromBasketButton.modal}
                                     onClickFunc={showSecondModal}
                                     additionalButtons={secondModalAdditionalButtons}
          />}
        </div>
    );

};

export default List;
