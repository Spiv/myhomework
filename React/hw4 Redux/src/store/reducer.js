const InitialStore = {
  productsData: [],
  firstModalOpen: false,
  secondModalOpen: false,
  addingInBasketCode: null,
  removingFromBasketCode: null,
  favoriteProducts: [],
  basket: []
};

const reducer = (store = InitialStore, action) => {
  switch (action.type) {
    case 'SAVE_CARDS':
      return {...store, productsData: action.payload};
    case 'TOGGLE_FIRST_MODAL':
      return {...store, firstModalOpen: !store.firstModalOpen};
    case 'TOGGLE_SECOND_MODAL':
      return {...store, secondModalOpen: !store.secondModalOpen};
    case 'SET_ADDING_IN_BASKET_CODE':
      return {...store, addingInBasketCode: action.payload};
    case 'SET_REMOVING_FROM_BASKET_CODE':
      return {...store, removingFromBasketCode: action.payload};
    case 'CHANGE_PRODUCTS_IN_BASKET':
      return {...store, basket: action.payload};
    case 'CHANGE_FAVORITE_PRODUCTS':
      return {...store, favoriteProducts: action.payload};
    default:
      return store
  }
};

export default reducer;