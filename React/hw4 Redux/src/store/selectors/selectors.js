export const getBasket = store => store.basket;
export const getCardsSelector = store => store.productsData;
export const getFirstModalOpen = store => store.firstModalOpen;
export const getSecondModalOpen = store => store.secondModalOpen;
export const getFavoriteProducts = store => store.favoriteProducts;
export const getAddingInBasketCode = store => store.addingInBasketCode;
export const getRemovingFromBasketCode = store => store.removingFromBasketCode;