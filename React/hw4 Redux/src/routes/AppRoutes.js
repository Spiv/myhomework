import React from 'react';
import { Route, Switch } from 'react-router-dom';
import List from "../pages/List/List";
import {useSelector} from "react-redux";
import {getBasket, getCardsSelector, getFavoriteProducts} from "../store/selectors/selectors";

const AppRoutes = () => {
  const productsData = useSelector(getCardsSelector);
  const basket = useSelector(getBasket);
  const favoriteProducts = useSelector(getFavoriteProducts);

  const allCodes = [];
  productsData.map( card => allCodes.push(card.code));

  return (
    <>
      <Switch>
        <Route exect path='/basket' render={()=> <List cardsCodes={basket}/>}/>
        <Route exect path='/favorites' render={()=> <List cardsCodes={favoriteProducts}/>}/>
        <Route path='/' render={()=> <List cardsCodes={allCodes}/>}/>
      </Switch>
    </>
  );
};

export default AppRoutes;