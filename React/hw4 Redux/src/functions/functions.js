export function setFirstVisitStorage(key) {
  let storageItem = JSON.parse(localStorage.getItem(key));

  if(storageItem === null) {
    storageItem = [];
    setLocalStorageArray(key);
  }

  return storageItem;
}

export const setLocalStorageArray = (name) => localStorage.setItem(name, JSON.stringify([]));

