import React, {useEffect} from 'react';
import './App.css';
import Header from "./components/Header/Header";
import AppRoutes from "./routes/AppRoutes";
import {useDispatch} from "react-redux";
import {changeFavoriteProductsAction, changeProductsInBasketAction, loadDataAction } from "./store/action";
import {setFirstVisitStorage} from "./functions/functions";

const App = () => {
  const dispatch = useDispatch();
  const changeProductsInBasket = products => dispatch(changeProductsInBasketAction(products));
  const changeFavoriteProducts = products => dispatch(changeFavoriteProductsAction(products));

  useEffect(() => {
    dispatch(loadDataAction())
  }, [dispatch]);

  useEffect(() => {
    const storageBasket = setFirstVisitStorage('basket');
    const storageFavoriteProducts = setFirstVisitStorage('favoriteProducts');

    changeProductsInBasket(storageBasket);
    changeFavoriteProducts(storageFavoriteProducts);
  }, []);

  return <>
          <Header/>
          <AppRoutes/>
        </>
};

export default App;
