import React, {Component} from 'react';
import './App.css';
import Body from "./components/Body/Body";

class App extends Component {

    state = {
        firstModalOpen: false,
        secondModalOpen: false
    };

    render() {
        return <Body showFirstModal={this.showFirstModal} showSecondModal={this.showSecondModal} data={this.state}/>
    }

    showFirstModal = () => this.setState({firstModalOpen: !this.state.firstModalOpen});

    showSecondModal = () => this.setState({secondModalOpen: !this.state.secondModalOpen});

}

export default App;
