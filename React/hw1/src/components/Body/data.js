const buttonsData = {
            firstButton: {
            name: 'firstButton',
            id: 'firstButtonId',
            backgroundColor: '#E74C5C',
            text: 'Open first window',
            modal: {
                header: 'Do you want to delete this file?',
                headerBackgroundColor: '#D44637',
                closeButton: true,
                backgroundColor: '#E74C3C',
                text: `If you delete this file, it wo'nt be possible to undo action.
                            Are you sure you want to delete this file? `,
                actions: [
                    {
                        id: 'okButton',
                        text: 'Ok',
                        backgroundColor: '#B3382C'
                    },
                    {
                        id: 'cancelButton',
                        text: 'Cancel',
                        backgroundColor: '#B3382C'
                    }
                ]
            }
        },
        secondButton: {
            name: 'secondButton',
            id: 'secondButtonId',
            backgroundColor: '#0F5DA5',
            text: 'Open second window',
            modal: {
                header: 'Modal 2 title',
                backgroundColor: '#2064EE',
                headerBackgroundColor: '#0F5DA8',
                closeButton: false,
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                actions: [
                    {
                        id: 'firstButton',
                        text: 'Create',
                        backgroundColor: 'red'
                    },
                    {
                        id: 'secondButton',
                        text: 'Create+',
                        backgroundColor: 'green'
                    }
                ]
            }
        }
};

export default buttonsData;
