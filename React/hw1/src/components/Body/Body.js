import React, {Component} from 'react';
import Button from "../Button/Button";
import PropTypes from 'prop-types';
import Modal from "../../pages/Modal/Modal";
import buttonsData from '../Body/data';

class Body extends Component {
    render() {

        const {showFirstModal, showSecondModal, data} = this.props;
        const {firstModalOpen, secondModalOpen} = data;
        const {firstButton, secondButton} = buttonsData;

        const firstModalAdditionalButtons = firstButton.modal.actions.map(button => {
            return <Button onClickFunc={() => showFirstModal()} key={button.id} description={button}/>
        });

        const secondModalAdditionalButtons = secondButton.modal.actions.map(button => {
            return <Button onClickFunc={() => showSecondModal()} key={button.id} description={button}/>
        });

        return (
            <>
                {firstModalOpen && <Modal modal={firstButton.modal}
                                          onClickFunc={showFirstModal}
                                          name={'firstModalOpen'}
                                          additionalButtons={firstModalAdditionalButtons}/>}
                {secondModalOpen && <Modal modal={secondButton.modal}
                                           onClickFunc={showSecondModal}
                                           name={'secondModalOpen'}
                                           additionalButtons={secondModalAdditionalButtons}/>}
                <Button onClickFunc={() => showFirstModal()} description={firstButton}/>
                <Button onClickFunc={() => showSecondModal()} description={secondButton}/>
            </>
        );
    }

}

Body.propTypes = {
    onClickFunc: PropTypes.func,
    firstButton: PropTypes.object,
    secondButton: PropTypes.object
};

export default Body;