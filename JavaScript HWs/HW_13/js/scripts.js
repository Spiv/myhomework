document.addEventListener('DOMContentLoaded', letsCheck);

function letsCheck () {

    if (localStorage.getItem('Key') === '1') {

        setStyle();

    }
}

const setStyle = function () {
    let elementsForMakingChanges;
    let arr;

    elementsForMakingChanges = document.getElementsByTagName('tr');
    arr = Array.from(elementsForMakingChanges);

    for (let i of arr) {

        if (i.classList.contains('dark-yellow-them')) {

            arr.forEach(element => element.classList.remove('dark-yellow-them'));
            localStorage.setItem('Key', '0');

        } else {

            arr.forEach(element => element.classList.add('dark-yellow-them'));
            localStorage.setItem('Key', '1');

        }
    }
};





