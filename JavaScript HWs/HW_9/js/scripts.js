document.addEventListener('DOMContentLoaded', onDOMLoaded);
let firstShow;

function onDOMLoaded () {
    firstShow = document.getElementById('Akali');
    firstShow.style.display = 'block';

    setActiveClass ();

}

function activeClassRemove () {
    const tab = document.getElementsByClassName('active');

    if (tab) {
        const styleForRemove = tab[0];

        styleForRemove.classList.toggle('active');
    }

}

function setActiveClass () {

    const clickedTab = document.getElementById('tabsUL');
    let show;

    clickedTab.addEventListener('click', function (event) {

        activeClassRemove ();

        if (firstShow) {
            firstShow.style.display = 'none';
        }

        if (show) {

            show.style.display = 'none';

        }

        const addClass = event.target;

        addClass.classList.add('active');

        show = document.getElementById(`${addClass.innerText}`);
        show.style.display = 'block';

        return show;
    });
}