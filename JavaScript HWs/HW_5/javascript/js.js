function createNewUser() {
    let newUser = {
        firstName: prompt("Enter ur name"),
        lastName: prompt("Enter ur last name"),
        dateOfBirth: prompt("enter ur date of birth"),

        getLogin: function () {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase()
        },

        getAge: function () {
            const nowDate = new Date();
            const year = this.dateOfBirth.slice(6);
            const month = this.dateOfBirth.slice(3, 5) - 1;
            const day = this.dateOfBirth.slice(0, 2);
            let age = nowDate.getFullYear() - year;
            if (month > nowDate.getMonth() || day > nowDate.getDate()) {
                age = age - 1;
            }
            return age;
        },

        getPassword: function() {
            return (this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.dateOfBirth.slice(6))
        }
    };

    return newUser
}

let user = createNewUser();

alert(user.getLogin());
alert(user.getAge());
alert(user.getPassword());



