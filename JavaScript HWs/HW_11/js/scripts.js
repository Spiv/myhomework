document.addEventListener('DOMContentLoaded', onDOMLoaded);

function onDOMLoaded () {
    let keyValue;
    let result;

    document.addEventListener('keydown', function(event){

        keyValue = event.key;

        if (keyValue !== 'Enter') {

            keyValue = keyValue.toUpperCase();

        }

        const allElementsByClass = document.getElementsByClassName('btn');

        for ( let i of allElementsByClass) {

            if(i.innerText === keyValue) {
                if (result) {

                    result.classList.remove('blue');

                }

                result = i;
                i.classList.add('blue');

            }

        }

    });




}