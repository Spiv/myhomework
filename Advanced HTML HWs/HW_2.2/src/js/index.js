document.addEventListener("DOMContentLoaded", myFunc);

function myFunc() {

    const hamburger = document.getElementById('hamburgerButton');


    hamburger.addEventListener('click', function() {
        const menu = document.getElementById('mobileMenu');
        menu.classList.toggle('hidden');

        const hamburgerLines = document.getElementById('hamburgerLines');
        const hamburgerX = document.getElementById('hamburgerX');

        hamburgerLines.classList.toggle('hidden');
        hamburgerX.classList.toggle('hidden');
    })

}